let container = document.getElementsByClassName('container')[0];
let menClothing = document.getElementById('men-clothing');
let womenClothing = document.getElementById('women-clothing');
let jewelery = document.getElementById('jewelery');
let electronics = document.getElementById('electronics');
const signupBtn = document.getElementById('signup');
const loginBtn = document.getElementById('login');

document.querySelector(".loader").style.display = 'block';
document.querySelector('.product').style.display = 'none';

let products = fetch("https://fakestoreapi.com/products")
    .then((response) => {
        return response.json();
    }).then((result) => {
        document.querySelector(".loader").style.display = 'none';
        document.querySelector('.product').style.display = 'block';
        if (result.length === 0) {
            container.innerHTML = "No product available";
        } else {
            for (let index = 0; index < result.length; index++) {
                const item = result[index];
                const itemDetails = document.createElement('div');
                const title = item.title.length < 50 ? item.title : item.title.slice(0, 32) + '...';
                itemDetails.innerHTML = `<div class="item-container">
                                            <h4 id="item-title">${title}</h4>
                                            <div id="image-container">
                                                <img id="item-image" height="280px" width="280px" src=${item.image}>
                                            </div>
                                            <div class="price-details">
                                            <p><b>Price:</b>$${item.price}</p>
                                            <p><b>Rating:</b>${item.rating.rate}</p>
                                            <div>
                                        </div>`;

                if (item.category === `men's clothing`) {
                    menClothing.append(itemDetails);
                } else if (item.category === `jewelery`) {
                    jewelery.append(itemDetails);
                } else if (item.category === `women's clothing`) {
                    womenClothing.append(itemDetails);
                } else if (item.category === `electronics`) {
                    electronics.append(itemDetails);
                }

                itemDetails.addEventListener('click', () => {
                    localStorage.setItem("id", item.id);
                    window.location.href = './detail/detail.html';
                });
            }

        }
    }).catch((error) => {
        container.innerHTML = `<h1>500:Error connecting to the server!!!</h1>`;
    });

signupBtn.addEventListener('click', () => {
    window.location.href = './signup/signup.html';
});

loginBtn.addEventListener('click', () => {
    window.location.href = './login/login.html';
});

