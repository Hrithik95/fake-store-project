const itemID = localStorage.getItem("id");
const productDetails = document.getElementById('product-details');
document.querySelector(".loader").style.display = 'block';
document.querySelector('#product-details').style.display = 'none';

document.addEventListener('DOMContentLoaded', () => {
    fetch(`https://fakestoreapi.com/products/${itemID}`)
        .then((response) => {
            return response.json();
        })
        .then((result) => {
            document.querySelector(".loader").style.display = 'none';
            document.querySelector('#product-details').style.display = 'flex';
            
            //create Elements
            const product = document.createElement('div');
            const productTitle = document.createElement('h2');
            const productImage = document.createElement('img');
            const productDescription = document.createElement('p');
            const productPrice = document.createElement('p');
            const productRating = document.createElement('p');
            const productReviewCount = document.createElement('p');

            //setAttributes
            product.setAttribute('class', 'product-container');
            productImage.setAttribute('src', `${result.image}`);
            productTitle.setAttribute('id', 'title');
            productDescription.setAttribute('id', 'description');
            productPrice.setAttribute('id', 'price');
            productRating.setAttribute('id', 'rating');
            productReviewCount.setAttribute('id', 'count');

            //add text
            productTitle.innerHTML = result.title;
            productDescription.innerHTML = result.description;
            productPrice.innerHTML = `Price:` + result.price;
            productRating.innerHTML = `Rating:` + result.rating.rate;
            productReviewCount.innerHTML = `Number of Reviews:` + result.rating.count;

            //append child
            product.append(productTitle);
            product.append(productImage);
            product.append(productPrice);
            product.append(productRating);
            product.append(productReviewCount);
            product.append(productDescription);
            productDetails.append(product);

        });
});