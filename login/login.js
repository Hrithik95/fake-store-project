let userDetails = localStorage.getItem('userInfo');
const submitBtn = document.getElementById('button');

let parsedUserData = JSON.parse(userDetails);

submitBtn.addEventListener('click',(event)=>{
    event.preventDefault();
    const loginEmail = document.getElementById('login-email').value;
    const loginPassword = document.getElementById('login-password').value;
    if(loginEmail === parsedUserData.email && loginPassword === parsedUserData.password){
        window.location.href = '../index.html';
    }else{
        document.getElementById('message').innerHTML = 'Incorrect Username or Password';
    }
});

