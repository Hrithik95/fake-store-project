const submitBtn = document.getElementById('button');
const message = document.getElementById('message');

submitBtn.addEventListener('click', (event) => {
    event.preventDefault();
    const emailId = document.getElementById('email').value;
    const passwordText = document.getElementById('password').value;
    const confirmPasswordText = document.getElementById('confirm-password').value;
    if (passwordText !== "" && passwordText === confirmPasswordText) {
        let userDetails = {
            'email': emailId,
            'password': passwordText
        }
        localStorage.setItem('userInfo', JSON.stringify(userDetails));
        window.location.href = "../login/login.html";
    } else {
        message.innerHTML = "SignUp Failed.Please try again.";
    }
});